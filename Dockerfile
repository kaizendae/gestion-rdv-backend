FROM docker.io/adoptopenjdk/openjdk11:alpine-slim
#RUN addgroup -S youcode && adduser -S youcode -G youcode
#USER youcode:youcode
EXPOSE 8000
COPY target/*.jar app.jar
ENTRYPOINT ["java","-jar","/app.jar"]