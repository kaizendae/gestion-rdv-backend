#!/usr/bin/env bash

TAG=${1}

export BUILD_NAME=${TAG}
mkdir ./k8s/.generated/
for f in ./k8s/backend/*.yaml
do
  envsubst < $f > "./k8s/.generated/$(basename $f)"
done

kubectl apply -f ./k8s/.generated/
